package com.movie.imovie.repository

import android.content.Context
import android.content.SharedPreferences

class TokenRepository(context: Context) {

    val sharedPreferences : SharedPreferences
    init {
        sharedPreferences = context.getSharedPreferences("secret", Context.MODE_PRIVATE)
    }

    suspend fun getToken(): String? {
        val token = sharedPreferences.getString("token", null)
        return token
    }

    suspend fun saveToken(token: String?) {
        sharedPreferences.edit().apply {
            putString("token", token)
            apply()
        }
    }
}