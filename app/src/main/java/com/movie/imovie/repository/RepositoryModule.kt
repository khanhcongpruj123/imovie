package com.movie.imovie.repository

import android.content.Context
import com.movie.imovie.service.di.ApiModule

object RepositoryModule {

    fun getMovieRepository(): MovieRepository {
        return MovieRepository(ApiModule.getMovieService())
    }

    fun getTokenRepository(context: Context): TokenRepository {
        return TokenRepository(context)
    }
}