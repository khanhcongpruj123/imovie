package com.movie.imovie.repository

import android.util.Log
import com.movie.imovie.model.MovieType
import com.movie.imovie.model.ViewStatus
import com.movie.imovie.service.movie.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieRepository(val movieApi: MovieApi) {

    suspend fun getTopMovieByType(type: MovieType) : List<com.movie.imovie.model.Movie> {
        val data = when(type) {
            MovieType.SCHOOL -> withContext(Dispatchers.IO) {
                movieApi.getTopTypeMovie("SCHOOL").execute().body()?.data
            }
            MovieType.DRAMA -> withContext(Dispatchers.IO) {
                movieApi.getTopTypeMovie("DRAMA").execute().body()?.data
            }
            MovieType.ROMANCE -> withContext(Dispatchers.IO) {
                movieApi.getTopTypeMovie("ROMANCE").execute().body()?.data
            }
            MovieType.HUMOR -> withContext(Dispatchers.IO) {
                movieApi.getTopTypeMovie("HUMOR").execute().body()?.data
            }
            else -> emptyList()
        }
        return data?.map {
            it.toMovieDomain()
        }?: emptyList()
    }

    suspend fun getMovieHistory(movieId: String, token: String) : List<com.movie.imovie.model.History> {

        return withContext(Dispatchers.IO) {
            val res = movieApi.getMovieHistory(movieId, "Bearer ${token}").execute().body()
            if (res != null) {
                res.data.map { it.toHistoryDomain() }
            } else {
                emptyList()
            }
        }
    }

    suspend fun getEpisodeHistory(episodeId: String, token: String) : com.movie.imovie.model.History? {
        return withContext(Dispatchers.IO) {
            val res = movieApi.getHistoryByEpisodeId(episodeId, "Bearer ${token}").execute().body()
            res?.data?.toHistoryDomain()
        }
    }

    suspend fun getDetailMovie(id: String) : com.movie.imovie.model.Movie? {
        return with(Dispatchers.IO) {
            val res = movieApi.getDetailMovie(id).execute().body()
            res?.data?.get(0)?.toMovieDomain()
        }
    }

    suspend fun getMovieByName(key: String) : List<com.movie.imovie.model.Movie> {
        return with(Dispatchers.IO) {
            val res = movieApi.getMovieByName(key).execute().body()
            res?.data?.map { it.toMovieDomain() }?: emptyList()
        }
    }

    suspend fun viewed(episodeId: String, token: String) {
        return withContext(Dispatchers.IO) {
            val res = movieApi.view(
                HistoryBody(
                    episodeId = episodeId,
                    viewStatus = 1,
                    currentViewing = 0
                ),
                "Bearer ${token}"
            ).execute().body()
//            Log.d("AppLog", res.toString())
        }
    }

    suspend fun viewing(episodeId: String, currentDuration: Long, token: String) {
        return withContext(Dispatchers.IO) {
            val res = movieApi.view(
                HistoryBody(
                    episodeId = episodeId,
                    viewStatus = 2,
                    currentViewing = currentDuration
                ),
                "Bearer ${token}"
            ).execute().body()
//            Log.d("AppLog", res.toString())
        }
    }

    suspend fun getWatchingMovie(token: String) = withContext(Dispatchers.IO) {
        movieApi.getListMovieWatching("Bearer ${token}").execute().body()?.data?.map {
            it.toMovieDomain()
        }
    }

    suspend fun getListMovieByType(type: MovieType) = withContext(Dispatchers.IO){
        val typeStr = type.toString()
        movieApi.getListMovieByType(typeStr).execute().body()?.data?.map { it.toMovieDomain() }?: emptyList()
    }

    suspend fun bookmark(movieId: String, token: String) = withContext(Dispatchers.IO) {
        movieApi.bookmarkMovie(BookmarkBody(movieId), "Bearer ${token}").execute().body()
    }

    suspend fun unbookmark(movieId: String, token: String)  = withContext(Dispatchers.IO) {
        movieApi.unbookmarkMovie(movieId, "Bearer ${token}").execute().body()
    }

    suspend fun getBookmarkByMovieId(movieId: String, token: String) = withContext(Dispatchers.IO) {
        movieApi.getBookmarkByMovieId(movieId, "Bearer ${token}").execute().body()
    }

    suspend fun getBookmarkMovie(token: String) = withContext(Dispatchers.IO) {
        movieApi.getBookmark("Bearer ${token}").execute().body()?.data?.map {
            getDetailMovie(it.movieId)!!
        }
    }

}

fun History.toHistoryDomain() = com.movie.imovie.model.History(
    userId = this.userId,
    movieId = this.movieId,
    episodeId = this.episodeId,
    viewStatus = this.viewStatus?.toViewStatus(),
    currentViewing = this.currentViewing

)

fun String.toViewStatus() = when(this) {
    "VIEWED" -> ViewStatus.VIEWED
    "VIEWING" -> ViewStatus.VIEWING
    else -> ViewStatus.NOT_VIEWED
}

fun Movie.toMovieDomain() = com.movie.imovie.model.Movie(
    id = this.id,
    name = this.name,
    subName = this.subName,
    description = this.description,
    thumbnailUrl = this.thumbnailUrl,
    duration = this.duration,
    listEpisode = this.listEpisode?.map {
        it.toEpisodeDomain()
    }?: emptyList(),
    view = this.view,
    type = this.type?.map {
        it.toMovieType()
    },
    link = this.link,
    updateDate = this.updateDate,
    publishDate = this.publishDate,
    numEpisode = this.numEpisode,
    director = this.director,
    isBookmark = this.isBookmark
)

fun Episode.toEpisodeDomain() = com.movie.imovie.model.Episode(
    id = this.id,
    name = this.name,
    link = this.link,
    linkPlay = this.linkPlay
)

fun String.toMovieType() = when(this) {
    "SCHOOL" -> MovieType.SCHOOL
    else -> MovieType.OTHER
}