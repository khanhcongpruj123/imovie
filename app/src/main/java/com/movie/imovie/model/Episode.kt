package com.movie.imovie.model

import java.io.Serializable

data class Episode(
    val id: String,
    val name: String?,
    val link: String?,
    val linkPlay: String?
) : Serializable