package com.movie.imovie.model

import com.google.gson.annotations.SerializedName

data class History(
    val userId: String,
    val movieId: String,
    val episodeId: String,
    val viewStatus: ViewStatus?,
    val currentViewing: Long?
)

enum class ViewStatus {
    //1
    VIEWED,
    //2
    VIEWING,
    //0
    NOT_VIEWED
}