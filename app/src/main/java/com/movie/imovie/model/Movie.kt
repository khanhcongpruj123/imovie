package com.movie.imovie.model

import java.io.Serializable

data class Movie(
    val id: String,
    val name: String?,
    val subName: String?,
    val description: String?,
    val duration: String?,
    val director: String?,
    val numEpisode: Int?,
    val publishDate: String?,
    val updateDate: String?,
    val thumbnailUrl: String?,
    val link: String?,
    val type: List<MovieType>?,
    val view: Int?,
    val listEpisode: List<Episode>?,
    val isBookmark: Boolean? = false
) : Serializable

enum class MovieType {
    SCHOOL,
    OTHER,
    DRAMA,
    ROMANCE,
    HUMOR
}