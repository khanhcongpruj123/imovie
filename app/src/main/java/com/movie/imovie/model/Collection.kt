package com.movie.imovie.model

data class Collection(
    val name: String,
    val listMovie: List<Movie>
)
