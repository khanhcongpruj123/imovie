package com.movie.imovie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.icongkhanh.xemphimmienphi.ui.home.HomeFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment();
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, homeFragment)
            .setPrimaryNavigationFragment(homeFragment)
            .commit()
    }
}