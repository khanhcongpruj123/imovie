package com.movie.imovie.utils

object ViewUtils {

    fun String?.validTextUi(): String? {
        return this?.trim()?.replace("\\n", "")
    }

}