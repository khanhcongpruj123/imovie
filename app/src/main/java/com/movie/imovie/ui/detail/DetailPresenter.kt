package com.movie.imovie.ui.detail

import android.util.Log
import com.movie.imovie.model.Movie
import com.movie.imovie.repository.MovieRepository
import com.movie.imovie.repository.TokenRepository
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.*

class DetailPresenter(val movieId: String, val movieRepository: MovieRepository, val tokenRepository: TokenRepository) : BasePresenter<DetailView>() {

    val loadMovieJob = Job(rootJob)
    val bookmarkJob = Job(rootJob)

    var movie: Movie? = null

    override fun onStart() {
        super.onStart()

        CoroutineScope(loadMovieJob).launch {

            movie = movieRepository.getDetailMovie(movieId)
            if (movie != null) {

                view.showDetailMovie(movie!!)
            }

            val token = tokenRepository.getToken()

            if (token != null) {
                val isBookmark = movieRepository.getBookmarkByMovieId(movieId, token)?.data
                val _m = movie?.copy(isBookmark = isBookmark)
                movie = _m
                view.showDetailMovie(movie!!)
                val history = movieRepository.getMovieHistory(movieId, token)
                if (history != null) view.showHistory(history)
            }
        }
    }

    fun bookmark() {
        CoroutineScope(bookmarkJob).launch {
            val token = withContext(Dispatchers.IO) { tokenRepository.getToken() }
            if (token != null) {
                if (movie?.isBookmark == false || movie?.isBookmark == null) {
                    movieRepository.bookmark(movieId, token)
                    val _m = movie?.copy(isBookmark = true)
                    movie = _m
                    view.showDetailMovie(movie!!)
                }
                else {
                    movieRepository.unbookmark(movieId, token)
                    val _m = movie?.copy(isBookmark = false)
                    movie = _m
                    view.showDetailMovie(movie!!)
                }
            }
        }
    }
}