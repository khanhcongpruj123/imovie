package com.icongkhanh.xemphimmienphi.model

data class EpisodeUI(
    val id: String,
    val name: String = "",
    var isViewed: Boolean = false,
    val onClick: () -> Unit
)