package com.icongkhanh.xemphimmienphi.ui.home

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.transition.MaterialElevationScale
import com.icongkhanh.xemphimmienphi.model.MovieCard
import com.icongkhanh.xemphimmienphi.model.MovieCollectionUI
import com.icongkhanh.xemphimmienphi.ui.category.CategoryFragment
import com.icongkhanh.xemphimmienphi.ui.detail.DetailFragment
import com.icongkhanh.xemphimmienphi.ui.search.SearchFragment
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentHomeBinding
import com.movie.imovie.model.Collection
import com.movie.imovie.model.Movie
import com.movie.imovie.model.MovieType
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.ui.BaseFragment
import com.movie.imovie.ui.detail.DetailView
import com.movie.imovie.ui.home.HomePresenter
import com.movie.imovie.ui.home.HomeView
import com.movie.imovie.ui.login.LoginFragment
import com.movie.imovie.ui.user.UserFragment
import com.movie.imovie.widget.collection.MovieCollectionView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeFragment : BaseFragment<HomeView, HomePresenter>(), HomeView {

    lateinit var binding : FragmentHomeBinding

    override fun onAttach(context: Context) {
        presenter = HomePresenter(RepositoryModule.getMovieRepository(), RepositoryModule.getTokenRepository(context))
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
//        Log.d("AppLog", "Resume")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        exitTransition = MaterialElevationScale(/* growing= */ false)
//        reenterTransition = MaterialElevationScale(/* growing= */ true)
//        enterTransition = MaterialElevationScale(true)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnSearch.setOnClickListener {
            val searchFragment = SearchFragment.newInstance()
            fragmentManager!!.beginTransaction().apply {
                add(R.id.fragment_container, searchFragment)
                addToBackStack(null)
                commit()
            }
        }

        binding.btnCategory.setOnClickListener {
            val categoryFragment = CategoryFragment.newInstance()
            fragmentManager!!.beginTransaction().apply {
                add(R.id.fragment_container, categoryFragment)
                addToBackStack(null)
                commit()
            }
        }

        binding.btnUser.setOnClickListener {
            presenter.onClickUserButton()
        }
    }

    override suspend fun showMiniPlaylist(miniPlayList: List<Collection>) {
        binding.container.apply {
            this.post {
                this.removeAllViews()
                miniPlayList.forEach { collection ->
                    val movieCollectionView = MovieCollectionView(requireContext())
                    val movieCollectionUi = MovieCollectionUI(
                        collection.name,
                        collection.listMovie.map { it.toMovieCard { view ->
                            presenter.gotoDetail(it)
                        } },
                        {

                        }
                    )
                    movieCollectionView.submitMovieCollection(movieCollectionUi)
                    val layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    this.addView(movieCollectionView, layoutParams)
                }
            }
        }
    }

    override suspend fun goToDetail(movie: Movie) {
        fragmentManager!!.beginTransaction()
            .replace(R.id.fragment_container, DetailFragment.newInstance(movie))
            .addToBackStack(null)
            .commit()
    }

    override suspend fun goToType(type: MovieType) {
        TODO("Not yet implemented")
    }

    override suspend fun showLoading(isShow: Boolean) {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.loading.root.visibility = if (isShow) View.VISIBLE else View.GONE
        }
    }

    override suspend fun goToLoginScreen() {
        withContext(Dispatchers.Main) {
            fragmentManager!!.beginTransaction()
                .replace(R.id.fragment_container, LoginFragment.newInstance())
                .addToBackStack(null)
                .commit()
        }
    }

    override suspend fun goToUserScreen() {
        withContext(Dispatchers.Main) {
            fragmentManager!!.beginTransaction()
                .replace(R.id.fragment_container, UserFragment.newInstance())
                .addToBackStack(null)
                .commit()
        }
    }

    override fun getViewContract(): HomeView {
        return this
    }
}

fun Movie.toMovieCard(onClickDetail: (view: View) -> Unit) = MovieCard(
    name = this.name?: "",
    subName = this.subName?: "",
    latestEpisode = "",
    thumbnailUrl = this.thumbnailUrl,
    onClickDetail = onClickDetail
)

fun Collection.toMovieCollectionUI(onClickSeeAll: () -> Unit, onClickDetail: (view: View) -> Unit) = MovieCollectionUI(
    name = this.name,
    listMovieCard = this.listMovie.map {
        it.toMovieCard(onClickDetail)
    },
    onClickSeeAll = onClickSeeAll
)