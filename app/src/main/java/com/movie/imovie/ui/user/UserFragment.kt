package com.movie.imovie.ui.user

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentUserBinding
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.ui.BaseFragment
import com.movie.imovie.ui.bookmark.BookmarkFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


/**
 * A simple [Fragment] subclass.
 * Use the [UserFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class UserFragment : BaseFragment<UserView, UserPresenter>(), UserView {

    lateinit var binding : FragmentUserBinding

    override fun onAttach(context: Context) {
        presenter = UserPresenter(RepositoryModule.getTokenRepository(context))
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLoggout.setOnClickListener {
            presenter.logout()
        }

        binding.btnBookmark.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .add(R.id.fragment_container, BookmarkFragment.newInstance())
                .addToBackStack(null)
                .commit()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            UserFragment()
    }

    override fun getViewContract(): UserView {
        return this
    }

    override suspend fun goBack() {
        withContext(Dispatchers.Main) {
            fragmentManager!!.popBackStack()
        }
    }

    override suspend fun goToBookmarkScreen() {
        // TODO
    }
}