package com.movie.imovie.ui.login

interface LoginView {
    suspend fun goBack()
    suspend fun showError(message: String)
    suspend fun showSuccess(message: String)
}