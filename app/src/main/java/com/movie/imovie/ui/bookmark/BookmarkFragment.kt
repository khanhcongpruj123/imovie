package com.movie.imovie.ui.bookmark

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.icongkhanh.xemphimmienphi.ui.detail.DetailFragment
import com.icongkhanh.xemphimmienphi.ui.home.toMovieCard
import com.icongkhanh.xemphimmienphi.ui.search.ListMovieCardSearchAdapter
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentBookmarkBinding
import com.movie.imovie.model.Movie
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.ui.BaseFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookmarkFragment : BaseFragment<BookmarkView, BookmarkPresenter>(), BookmarkView {

    lateinit var binding : FragmentBookmarkBinding

    lateinit var rcvadapter : ListMovieCardSearchAdapter

    override fun onAttach(context: Context) {
        presenter = BookmarkPresenter(RepositoryModule.getMovieRepository(), RepositoryModule.getTokenRepository(requireContext()))
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBookmarkBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rcvadapter = ListMovieCardSearchAdapter()
        binding.listBookmark.apply {
            layoutManager = GridLayoutManager(requireContext(), 4)
            adapter = rcvadapter
        }

        binding.btnBack.setOnClickListener {
            fragmentManager!!.popBackStack()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            BookmarkFragment()
    }

    override fun getViewContract(): BookmarkView {
        return this
    }

    override fun show(list: List<Movie>) {
        lifecycleScope.launch(Dispatchers.Main) {
            rcvadapter.submitList(list.map {
                it.toMovieCard { view ->
                    lifecycleScope.launch {
                        fragmentManager!!.beginTransaction()
                            .add(R.id.fragment_container, DetailFragment.newInstance(it))
                            .addToBackStack(null)
                            .commit()
                    }
                }
            })
        }
    }
}