package com.movie.imovie.ui.login

import android.content.Context
import android.os.Binder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentLoginBinding
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.service.di.ApiModule
import com.movie.imovie.ui.BaseFragment
import com.movie.imovie.ui.register.RegisterFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginFragment : BaseFragment<LoginView, LoginPresenter>(), LoginView {

    lateinit var binding : FragmentLoginBinding

    override fun onAttach(context: Context) {
        presenter = LoginPresenter(RepositoryModule.getTokenRepository(context), ApiModule.getLoginService())
        super.onAttach(context)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cirLoginButton.setOnClickListener {
            presenter.login(
                binding.editTextEmail.text.toString(),
                binding.editTextPassword.text.toString()
            )
        }

        binding.btnRegister.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main) {
                fragmentManager!!.beginTransaction()
                    .add(R.id.fragment_container, RegisterFragment.newInstance())
                    .addToBackStack(null)
                    .commit()
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            LoginFragment()
    }

    override fun getViewContract(): LoginView {
        return this
    }

    override suspend fun goBack() {
        CoroutineScope(Dispatchers.Main).launch {
            fragmentManager!!.popBackStack()
        }
    }

    override suspend fun showError(message: String) {
        view?.post {
            Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
        }
    }

    override suspend fun showSuccess(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }
}