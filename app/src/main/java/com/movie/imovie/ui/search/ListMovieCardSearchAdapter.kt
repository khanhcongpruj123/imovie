package com.icongkhanh.xemphimmienphi.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.icongkhanh.xemphimmienphi.model.MovieCard
import com.icongkhanh.xemphimmienphi.ui.moviecollection.ListMovieAdapter
import com.movie.imovie.R
import com.movie.imovie.databinding.ItemMovieCardGridBinding

class ListMovieCardSearchAdapter : ListAdapter<MovieCard, ListMovieCardSearchAdapter.MovieHolder>(ListMovieAdapter.MovieCardDiff) {

    inner class MovieHolder(val binding: ItemMovieCardGridBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MovieCard) {
            binding.thumbnail.load(item.thumbnailUrl?.toUri()) {
                crossfade(true)
                placeholder(R.drawable.ic_movie)
            }
            binding.name.text = item.name
            binding.subname.text = item.subName
            itemView.setOnClickListener {
                item.onClickDetail(binding.root)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val binding = ItemMovieCardGridBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.bind(getItem(position))
    }
}