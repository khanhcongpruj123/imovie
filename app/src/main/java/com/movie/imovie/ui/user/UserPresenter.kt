package com.movie.imovie.ui.user

import com.movie.imovie.repository.TokenRepository
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserPresenter(val tokenRepository: TokenRepository) : BasePresenter<UserView>() {

    fun logout() {
        CoroutineScope(rootJob).launch {
            withContext(Dispatchers.IO) { tokenRepository.saveToken(null) }
            view.goBack()
        }
    }
}