package com.movie.imovie.ui.home

import com.movie.imovie.model.Movie
import com.movie.imovie.model.MovieType
import com.movie.imovie.model.Collection

interface HomeView {

    suspend fun showMiniPlaylist(miniPlayList: List<Collection>)
    suspend fun goToDetail(movie: Movie)
    suspend fun goToType(type: MovieType)
    suspend fun showLoading(isShow: Boolean)
    suspend fun goToLoginScreen()
    suspend fun goToUserScreen()
}