package com.movie.imovie.ui.player

import android.app.PictureInPictureParams
import android.app.RemoteAction
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.util.MimeTypes
import com.movie.imovie.databinding.ActivityPlayerBinding
import com.movie.imovie.model.ViewStatus
import com.movie.imovie.repository.RepositoryModule
import kotlinx.coroutines.*
import okhttp3.Dispatcher


class PlayerActivity : AppCompatActivity() {

    lateinit var binding : ActivityPlayerBinding
    lateinit var player : SimpleExoPlayer

    val job = Job()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val episodeId = intent.getStringExtra("episode_id")
        val linkPlay = intent.getStringExtra("link_play")

        if (linkPlay == null) {
            finish()
        } else {

            val movieRepository = RepositoryModule.getMovieRepository()
            val tokenRepository = RepositoryModule.getTokenRepository(this)

            player = SimpleExoPlayer.Builder(this)
                .build()

            player.addListener(object : Player.Listener {
                override fun onPlaybackStateChanged(state: Int) {
                    super.onPlaybackStateChanged(state)

                    if (state == Player.STATE_ENDED) {
                        CoroutineScope(Dispatchers.IO).launch {
                            val token = tokenRepository.getToken();
                            if (episodeId != null && token != null) movieRepository.viewed(episodeId, token)
                        }
                    }
                }
            })

            CoroutineScope(Dispatchers.IO).launch {
                val token = tokenRepository.getToken()
                if (episodeId != null && token != null) {
                    val history = movieRepository.getEpisodeHistory(episodeId, token)
                    if (history != null && history.viewStatus == ViewStatus.VIEWING && history.currentViewing != null) {
                        withContext(Dispatchers.Main) {
                            player.seekTo(history.currentViewing)
                        }
                    }
                }
            }

            CoroutineScope(Dispatchers.IO + job).launch {
                while (true) {
                    if (withContext(Dispatchers.Main){ player.isPlaying }) {
                        val token = tokenRepository.getToken()
                        val duration = withContext(Dispatchers.Main) { player.currentPosition }
                        if (episodeId != null && token != null) movieRepository.viewing(episodeId, duration, token)
                        delay(1000)
                    }
                }
            }

            binding.playerView.player = player

            val mediaItem = MediaItem.Builder()
                .setUri(linkPlay.toUri())
                .setMimeType(MimeTypes.APPLICATION_M3U8)
                .build()

            val defaultHttpDatSourceFactory = DefaultHttpDataSource.Factory()
                .setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36")
                .setDefaultRequestProperties(
                    mapOf(
                        "Referer" to "https://dongphym.net",
                        "origin" to "https://dongphym.net"
                    )
                )
            val hlsMediaSource = HlsMediaSource.Factory(defaultHttpDatSourceFactory)
                .createMediaSource(mediaItem)

            player.setMediaSource(hlsMediaSource)
            player.playWhenReady = true
            player.prepare()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && packageManager.hasSystemFeature(PackageManager.FEATURE_PICTURE_IN_PICTURE)) {
            enterPictureInPictureMode(
                PictureInPictureParams.Builder()
                    .build()
            )
        }
    }

    override fun onDestroy() {

        job.cancel()
        player.stop()
        player.release()

        super.onDestroy()
    }
}