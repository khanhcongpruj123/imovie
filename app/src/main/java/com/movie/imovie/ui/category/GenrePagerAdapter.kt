package com.icongkhanh.xemphimmienphi.ui.category

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.movie.imovie.model.MovieType

class GenrePagerAdapter(val fm: FragmentManager, val listGenre: List<MovieType>) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val listGenreFragment = listGenre.map {
        ListMovieFragment.newInstance(it)
    }

    override fun getCount(): Int {
        return listGenre.size
    }

    override fun getItem(position: Int): Fragment {
        return listGenreFragment[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return listGenre[position].name
    }
}