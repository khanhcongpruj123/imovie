package com.movie.imovie.ui.search

import com.movie.imovie.repository.MovieRepository
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SearchPresenter(val movieRepository: MovieRepository) : BasePresenter<SearchView>() {

    var searchJob = Job(rootJob)

    fun search(key: String) {
        searchJob.cancel()
        searchJob = Job(rootJob)
        CoroutineScope(rootJob).launch {
            view.showLoading(true)
            val listMovie = movieRepository.getMovieByName(key)
            view.showResult(listMovie)
            view.showLoading(false)
        }
    }
}