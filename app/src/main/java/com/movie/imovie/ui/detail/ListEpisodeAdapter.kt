package com.icongkhanh.xemphimmienphi.ui.detail

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.icongkhanh.xemphimmienphi.model.EpisodeUI
import com.movie.imovie.R
import com.movie.imovie.databinding.ItemEpisodeBinding
import com.movie.imovie.model.History
import com.movie.imovie.model.ViewStatus

class ListEpisodeAdapter(val context: Context) : ListAdapter<EpisodeUI, ListEpisodeAdapter.EpisodeHolder>(EpisodeDiff) {

    inner class EpisodeHolder(val binding: ItemEpisodeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: EpisodeUI) {
            binding.name.text = item.name.trim().replace("\\n", "")
            itemView.setOnClickListener {
                item.onClick()
            }
            if (!item.isViewed) {
                binding.name.background = context.getDrawable(R.drawable.bg_episode_not_viewed)
            } else {
                binding.name.background = context.getDrawable(R.drawable.bg_episode_viewed)
            }
        }
    }

    object EpisodeDiff : DiffUtil.ItemCallback<EpisodeUI>() {
        override fun areItemsTheSame(oldItem: EpisodeUI, newItem: EpisodeUI): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: EpisodeUI, newItem: EpisodeUI): Boolean {
            return oldItem.name == newItem.name
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeHolder {
        val binding = ItemEpisodeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EpisodeHolder(binding)
    }

    override fun onBindViewHolder(holder: EpisodeHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun updateHistory(history: List<History>) {
        history.forEach { h ->
            currentList.find { it.id == h.episodeId }.apply {
                this?.isViewed = h.viewStatus == ViewStatus.VIEWED
                notifyItemChanged(currentList.indexOf(this))
            }
        }
    }
}