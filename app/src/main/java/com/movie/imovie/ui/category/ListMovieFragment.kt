package com.icongkhanh.xemphimmienphi.ui.category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.icongkhanh.xemphimmienphi.ui.detail.DetailFragment
import com.icongkhanh.xemphimmienphi.ui.home.toMovieCard
import com.icongkhanh.xemphimmienphi.ui.search.ListMovieCardSearchAdapter
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentListMovieBinding
import com.movie.imovie.model.Movie
import com.movie.imovie.model.MovieType
import com.movie.imovie.repository.RepositoryModule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListMovieFragment : Fragment() {

    companion object {
        fun newInstance(type: MovieType): ListMovieFragment {
            val args = Bundle()
            args.putString("type", type.toString())
            val fragment = ListMovieFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var binding : FragmentListMovieBinding

    private lateinit var listMovieAdapter: ListMovieCardSearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentListMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        listMovieAdapter = ListMovieCardSearchAdapter()
        binding.listMovie.apply {
            adapter = listMovieAdapter

            val gridLayoutManager = GridLayoutManager(requireContext(), 4)
            layoutManager = gridLayoutManager

        }

        // load list movie
        lifecycleScope.launch {

            val type = arguments?.getString("type")!!.let {
                MovieType.valueOf(it)
            }
            val movieRepository = RepositoryModule.getMovieRepository()

            try {
                binding.loading.post { binding.loading.visibility = View.VISIBLE }

                val _listMovie = withContext(Dispatchers.IO) { movieRepository.getListMovieByType(type) }
                addMoviesToList(_listMovie)

                binding.loading.post { binding.loading.visibility = View.GONE }
            } catch (ex: Exception) {
                ex.printStackTrace()
                binding.loading.post { binding.loading.visibility = View.GONE }
            }
        }
    }

    private fun addMoviesToList(_listMovie : List<Movie>) {
        val listMovieUI = _listMovie.map { it.toMovieCard { view ->
            //xu li khi nhan vao phim
            val detailFragment = DetailFragment.newInstance(it)
            parentFragment?.fragmentManager?.beginTransaction()?.apply {

                setReorderingAllowed(true)
                add(R.id.fragment_container, detailFragment)
                addToBackStack(null)
                commit()
            }
        } }
        binding.root.postDelayed({
            listMovieAdapter.submitList(listMovieUI)
        }, 200)
    }
}