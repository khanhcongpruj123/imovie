package com.icongkhanh.xemphimmienphi.ui.search

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import com.google.android.material.transition.platform.MaterialElevationScale
import com.icongkhanh.xemphimmienphi.ui.detail.DetailFragment
import com.icongkhanh.xemphimmienphi.ui.home.toMovieCard
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentSearchBinding
import com.movie.imovie.model.Movie
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.ui.BaseFragment
import com.movie.imovie.ui.search.SearchPresenter
import com.movie.imovie.ui.search.SearchView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchFragment : BaseFragment<SearchView, SearchPresenter>(), SearchView{

    companion object {
        fun newInstance(): SearchFragment{
            val fragment = SearchFragment()
            return fragment
        }
    }


    private lateinit var listMovieAdapter : ListMovieCardSearchAdapter

    private lateinit var binding: FragmentSearchBinding

    override fun onAttach(context: Context) {
        presenter = SearchPresenter(RepositoryModule.getMovieRepository())
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        enterTransition = MaterialElevationScale(true)
//        exitTransition = MaterialElevationScale(false)
//        reenterTransition = MaterialElevationScale(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listMovieAdapter = ListMovieCardSearchAdapter()

        binding.listMovie.apply {
            adapter = listMovieAdapter
        }

        binding.loading.root.setBackgroundColor(Color.TRANSPARENT)

        binding.btnBack.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        binding.editQuery.doOnTextChanged { text, start, before, count ->
                presenter.search(text.toString())
        }
    }

    override fun getViewContract(): SearchView {
        return this
    }

    override suspend fun showResult(listMovie: List<Movie>) {
        binding.listMovie.post {
            listMovieAdapter.submitList(listMovie.map { it.toMovieCard { view ->
                CoroutineScope(Dispatchers.Main).launch {
                    goToDetail(it)
                }
            } }) {
                // den vi tri dau tien sau khi da update list xong
                binding.listMovie.scrollToPosition(0)
            }
        }
    }

    override suspend fun goToDetail(movie: Movie) {
        fragmentManager!!.beginTransaction()
            .add(R.id.fragment_container, DetailFragment.newInstance(movie))
            .addToBackStack(null)
            .commit()
    }

    override suspend fun showLoading(isShow: Boolean) {
        view?.post {
            binding.loading.root.visibility = if (isShow) View.VISIBLE else View.GONE
        }
    }
}