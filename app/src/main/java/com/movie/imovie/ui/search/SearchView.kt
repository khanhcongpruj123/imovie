package com.movie.imovie.ui.search

import com.movie.imovie.model.Movie

interface SearchView {

    suspend fun showResult(listMovie: List<Movie>)
    suspend fun goToDetail(movie: Movie)
    suspend fun showLoading(isShow: Boolean)
}