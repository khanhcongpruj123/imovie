package com.movie.imovie.ui.register

import com.movie.imovie.repository.TokenRepository
import com.movie.imovie.service.LoginApi
import com.movie.imovie.service.RegisterBody
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegisterPresenter(val loginApi: LoginApi, val tokenRepository: TokenRepository) : BasePresenter<RegisterView>() {


    fun register(username: String?, password: String?, phone: String?) {
        CoroutineScope(rootJob).launch {
            try {
                valid(username, password, phone)
                val data = withContext(Dispatchers.IO) { loginApi.registerWithUsernameAndPassword(
                    RegisterBody(username!!, password!!, phone!!)
                ).execute().body() }
                if (data?.data?.token == null) {
                    view.showMessage("Đăng nhập thất bại")
                } else {
                    tokenRepository.saveToken(data.data.token)
                    view.goToRoot()
                }

            } catch (ex: IllegalArgumentException) {
                ex.printStackTrace()
                view.showMessage("Hãy nhập đầy đủ thông tin")
            }
        }
    }

    fun valid(username: String?, password: String?, phone: String?) {
        if (username.isNullOrEmpty() || password.isNullOrEmpty() || phone.isNullOrEmpty()) {
            throw IllegalArgumentException()
        }
    }

}