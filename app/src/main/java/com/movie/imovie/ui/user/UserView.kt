package com.movie.imovie.ui.user

interface UserView {
    suspend fun goBack()
    suspend fun goToBookmarkScreen()
}