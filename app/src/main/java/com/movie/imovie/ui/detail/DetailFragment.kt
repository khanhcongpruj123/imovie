package com.icongkhanh.xemphimmienphi.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.BlurTransformation
import com.google.android.material.transition.MaterialElevationScale
import com.icongkhanh.xemphimmienphi.model.EpisodeUI
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentDetailBinding
import com.movie.imovie.model.Episode
import com.movie.imovie.model.History
import com.movie.imovie.model.Movie
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.ui.BaseFragment
import com.movie.imovie.ui.detail.DetailPresenter
import com.movie.imovie.ui.detail.DetailView
import com.movie.imovie.ui.player.PlayerActivity
import com.movie.imovie.utils.ViewUtils.validTextUi

class DetailFragment : BaseFragment<DetailView, DetailPresenter>(), DetailView {

    companion object {

        @JvmStatic
        fun newInstance(movie: Movie): DetailFragment{
            val args = Bundle()
            args.putSerializable("movie", movie)
            val fragment = DetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var episodeAdapter : ListEpisodeAdapter

    private lateinit var binding: FragmentDetailBinding

    override fun onAttach(context: Context) {

        val movie = arguments!!.getSerializable("movie") as Movie

        presenter = DetailPresenter(movie.id, RepositoryModule.getMovieRepository(), RepositoryModule.getTokenRepository(context))
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        enterTransition = MaterialElevationScale(true)
        reenterTransition = MaterialElevationScale(true)
        exitTransition = MaterialElevationScale(false)

//        // hoan lai enter animation
//        postponeEnterTransition()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        episodeAdapter = ListEpisodeAdapter(requireContext())

        binding.listEpisode.apply {
            adapter = episodeAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        }

        binding.btnBack.setOnClickListener {
            // tro lai man hinh truoc
            fragmentManager?.popBackStack()
        }

        binding.btnBookmark.setOnClickListener {
            presenter.bookmark()
        }
    }

    override fun getViewContract(): DetailView {
        return this
    }

    override suspend fun showDetailMovie(movie: Movie) {
        view?.post {
            binding.largeThumbnail.load(movie.thumbnailUrl?.toUri()) {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_foreground)
                transformations(BlurTransformation(requireContext(), 25f))
            }
            binding.thumbnail.load(movie.thumbnailUrl?.toUri()) {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_foreground)
            }
            binding.name.text = movie.name.validTextUi()
            binding.subname.text = movie.subName.validTextUi()
            binding.duration.text = movie.duration.validTextUi()
            binding.releaseDate.text = movie.publishDate.validTextUi()
            binding.numEpisode.text = (movie.numEpisode.toString() + " " + requireContext().getString(R.string.episode)).validTextUi()
            binding.viewCount.text = movie.view?.toString()?: "0"

            episodeAdapter.submitList(movie.listEpisode?.map { it.toEpisodeUI {
                startActivity(
                    Intent(requireContext(), PlayerActivity::class.java).apply {
                        putExtra("link_play", it.linkPlay)
                        putExtra("episode_id", it.id)
                    }
                )
            } })

            binding.btnBookmark.setImageResource( if (movie.isBookmark == false) R.drawable.ic_baseline_bookmark_border_24 else R.drawable.ic_baseline_bookmark_24 )
        }
    }

    override suspend fun showHistory(history: List<History>) {
        binding.listEpisode.post {
            episodeAdapter.updateHistory(history)
        }
    }
}

fun Episode.toEpisodeUI(onClick: () -> Unit) = EpisodeUI(
    id = this.id,
    name =this.name?: "",
    onClick = onClick
)