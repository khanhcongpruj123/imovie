package com.movie.imovie.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.movie.imovie.service.movie.Movie

abstract class BaseFragment<V : Any, P : BasePresenter<V>> : Fragment() {

    lateinit var presenter : P

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter.attach(getViewContract())
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart()
    }

    abstract fun getViewContract() : V

    override fun onDestroy() {
        super.onDestroy()
        presenter.stop()
    }
}