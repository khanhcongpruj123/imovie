package com.movie.imovie.ui.bookmark

import com.movie.imovie.model.Movie


interface BookmarkView {

    fun show(list: List<Movie>)
}