package com.movie.imovie.ui.home

import android.util.Log
import com.movie.imovie.model.Collection
import com.movie.imovie.model.MovieType
import com.movie.imovie.repository.MovieRepository
import com.movie.imovie.repository.TokenRepository
import com.movie.imovie.service.movie.Movie
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.*
import java.net.SocketTimeoutException

class HomePresenter(val movieRepository: MovieRepository, val tokenRepository: TokenRepository) : BasePresenter<HomeView>() {


    override fun onStart() {
        super.onStart()

        //load collection
//        Log.d("AppLog","Load list movie!")
        val loadCollectionJob = Job(rootJob)
        CoroutineScope(loadCollectionJob).launch {
            try {
                view.showLoading(true)
                val listCollection = mutableListOf<Collection>()
                val token = withContext(Dispatchers.IO) { tokenRepository.getToken() }
                if (token != null) {
                    listCollection.add(
                        Collection(
                            name = "Phim đang xem",
                            listMovie = movieRepository.getWatchingMovie(token)?: emptyList()
                        )
                    )
                }
                val schoolCollection = Collection(
                    name = "Học đường",
                    listMovie = movieRepository.getTopMovieByType(MovieType.SCHOOL)
                )
                val dramaCollection = Collection(
                    name = "Drama",
                    listMovie = movieRepository.getTopMovieByType(MovieType.DRAMA)
                )
                val romanceCollection = Collection(
                    name = "Lãng mạn",
                    listMovie = movieRepository.getTopMovieByType(MovieType.ROMANCE)
                )
                val humorCollection = Collection(
                    name = "Hài hước",
                    listMovie = movieRepository.getTopMovieByType(MovieType.HUMOR)
                )
                listCollection.add(schoolCollection)
                listCollection.add(dramaCollection)
                listCollection.add(romanceCollection)
                listCollection.add(humorCollection)
                view.showMiniPlaylist(listCollection)
                view.showLoading(false)
            } catch (ex: SocketTimeoutException) {
                ex.printStackTrace()
            }
        }
    }

    fun gotoDetail(movie: com.movie.imovie.model.Movie) {
        CoroutineScope(Dispatchers.Main).launch {
            view.goToDetail(movie)
        }
    }

    fun onClickUserButton() {
        CoroutineScope(rootJob).launch {
            val token = withContext(Dispatchers.IO) { tokenRepository.getToken() }
            if (token == null) {
                view.goToLoginScreen()
            } else {
                view.goToUserScreen()
            }
        }
    }
}