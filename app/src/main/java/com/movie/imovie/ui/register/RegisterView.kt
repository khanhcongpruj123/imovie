package com.movie.imovie.ui.register

interface RegisterView {

    suspend fun goToRoot()
    suspend fun goToLoginScreen()
    suspend fun showMessage(s: String)
}