package com.movie.imovie.ui.detail

import com.movie.imovie.model.History
import com.movie.imovie.model.Movie

interface DetailView {

    suspend fun showDetailMovie(movie: Movie)
    suspend fun showHistory(history: List<History>)
}