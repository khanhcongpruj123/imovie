package com.movie.imovie.ui.bookmark

import com.movie.imovie.repository.MovieRepository
import com.movie.imovie.repository.TokenRepository
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BookmarkPresenter(val movieRepository: MovieRepository, val tokenRepository: TokenRepository) : BasePresenter<BookmarkView>() {

    override fun onStart() {
        super.onStart()

        CoroutineScope(rootJob).launch {
            val listMovie = withContext(Dispatchers.IO) {
                val token = withContext(Dispatchers.IO) { tokenRepository.getToken() }
                if (token != null) {
                    val listMovie = movieRepository.getBookmarkMovie(token)
                    if (listMovie != null) {
                        view.show(listMovie?: emptyList())
                    }
                }
            }
        }
    }
}