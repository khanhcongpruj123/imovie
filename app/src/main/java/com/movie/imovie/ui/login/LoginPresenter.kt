package com.movie.imovie.ui.login

import com.movie.imovie.repository.TokenRepository
import com.movie.imovie.service.LoginApi
import com.movie.imovie.service.LoginBody
import com.movie.imovie.ui.BasePresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class LoginPresenter(val tokenRepository: TokenRepository, val loginApi: LoginApi) : BasePresenter<LoginView>() {

    fun login(username: String, password: String) {
        CoroutineScope(rootJob).launch {
            try {
                validLoginInfo(username, password)
                val loginRes = loginApi.loginWithCredential(LoginBody(username, password)).execute().body()
                if (loginRes != null && loginRes.errorCode == 0) {
                    with(Dispatchers.IO) {
                        tokenRepository.saveToken(loginRes.data.token)
                    }
                    view.goBack()
                } else {
                    view.showError("Đăng nhập thất bại")
                }
            } catch (ex: IllegalArgumentException) {
                ex.printStackTrace()
                view.showError("Hãy nhập tên đăng nhập và mật khẩu!")
            }
        }
    }

    private fun validLoginInfo(username: String?, password: String?) {
        if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            throw IllegalArgumentException("Username and password is required!")
        }
    }

}