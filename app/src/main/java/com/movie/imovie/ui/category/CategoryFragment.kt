package com.icongkhanh.xemphimmienphi.ui.category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.google.android.material.transition.MaterialElevationScale
import com.icongkhanh.xemphimmienphi.ui.search.SearchFragment
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentCategoryBinding
import com.movie.imovie.model.MovieType
import com.movie.imovie.repository.MovieRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CategoryFragment : Fragment() {

    companion object {

        @JvmStatic
        fun newInstance(): CategoryFragment {
            val args = Bundle()

            val fragment = CategoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var binding: FragmentCategoryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enterTransition = MaterialElevationScale(true)
        exitTransition = MaterialElevationScale(false)
        reenterTransition = MaterialElevationScale(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnBack.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        binding.btnSearch.setOnClickListener {
            fragmentManager!!.beginTransaction().apply {
                val searchFragment = SearchFragment()

                setReorderingAllowed(true)
                add(R.id.fragment_container, searchFragment)
                addToBackStack(null)
                commit()
            }
        }

        binding.tabBar.setupWithViewPager(binding.pager)
        binding.pager.offscreenPageLimit = 3

        binding.pager.apply {
            lifecycleScope.launch {
                val listGenre = withContext(Dispatchers.IO) {
                    listOf(
                        MovieType.HUMOR,
                        MovieType.ROMANCE,
                        MovieType.DRAMA,
                        MovieType.SCHOOL
                    )
                }
                adapter = GenrePagerAdapter(childFragmentManager, listGenre)
            }
        }
    }

}