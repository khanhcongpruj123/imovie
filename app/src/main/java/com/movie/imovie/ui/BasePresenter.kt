package com.movie.imovie.ui

import kotlinx.coroutines.Job

open abstract class BasePresenter<V : Any>{

    lateinit var view : V
    val rootJob = Job()

    open fun attach(view: V) {
        this.view = view
    }

    open fun onStart() {

    }

    open fun stop() {
        rootJob.cancel()
    }
}