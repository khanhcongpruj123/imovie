package com.movie.imovie.ui.register

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.icongkhanh.xemphimmienphi.ui.home.HomeFragment
import com.movie.imovie.R
import com.movie.imovie.databinding.FragmentRegisterBinding
import com.movie.imovie.repository.RepositoryModule
import com.movie.imovie.service.di.ApiModule
import com.movie.imovie.ui.BaseFragment
import com.movie.imovie.utils.ViewUtils.validTextUi
import kotlinx.coroutines.launch

class RegisterFragment : BaseFragment<RegisterView, RegisterPresenter>(), RegisterView {

    lateinit var binding : FragmentRegisterBinding

    override fun onAttach(context: Context) {
        presenter = RegisterPresenter(ApiModule.getLoginService(), RepositoryModule.getTokenRepository(context))
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cirLoginButton.setOnClickListener {
            presenter.register(
                binding.editTextEmail.text.toString().validTextUi(),
                binding.editTextPassword.text.toString().validTextUi(),
                binding.editTextMobile.text.toString().validTextUi()
            )
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            RegisterFragment()
    }

    override fun getViewContract(): RegisterView {
        return this
    }

    override suspend fun goToRoot() {
        fragmentManager!!.popBackStack()
    }

    override suspend fun goToLoginScreen() {
        fragmentManager!!.popBackStack()
    }

    override suspend fun showMessage(s: String) {
        lifecycleScope.launch {
            Toast.makeText(requireContext(), s, Toast.LENGTH_LONG).show()
        }
    }
}