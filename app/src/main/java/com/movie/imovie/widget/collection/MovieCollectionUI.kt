package com.icongkhanh.xemphimmienphi.model

data class MovieCollectionUI(
    val name: String,
    val listMovieCard: List<MovieCard>,
    val onClickSeeAll: () -> Unit
)