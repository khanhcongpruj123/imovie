package com.movie.imovie.widget.collection

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.icongkhanh.xemphimmienphi.model.MovieCollectionUI
import com.icongkhanh.xemphimmienphi.ui.moviecollection.ListMovieAdapter
import com.movie.imovie.databinding.MovieCollectionBinding

class MovieCollectionView : FrameLayout {

    private var binding: MovieCollectionBinding
    private var listMovieAdapter: ListMovieAdapter
    var movieCollection : MovieCollectionUI? = null

    constructor(context: Context) : super(context) {

    }

    init {

        binding = MovieCollectionBinding.inflate(LayoutInflater.from(context))

        listMovieAdapter = ListMovieAdapter()
        binding.collectionMovie.adapter = listMovieAdapter
        binding.collectionMovie.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        addView(binding.root)
    }

    fun submitMovieCollection(movieCollection: MovieCollectionUI) {

        this.movieCollection = movieCollection

        listMovieAdapter.submitList(movieCollection.listMovieCard)
        binding.name.text = movieCollection.name

        binding.btnSeeAll.setOnClickListener {
            movieCollection.onClickSeeAll()
        }
    }
}