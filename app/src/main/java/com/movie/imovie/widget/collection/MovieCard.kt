package com.icongkhanh.xemphimmienphi.model

import android.view.View
import com.movie.imovie.model.Movie

data class MovieCard(
    val name: String,
    val subName: String,
    val latestEpisode: String,
    val thumbnailUrl: String?,
    val onClickDetail: (view: View) -> Unit
)
