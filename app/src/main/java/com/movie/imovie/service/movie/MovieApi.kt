package com.movie.imovie.service.movie

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.*

interface MovieApi {

    @GET("movies/type/top")
    fun getTopTypeMovie(@Query("type") type: String) : Call<GetTopFilmResponse>

    @GET("movies/{movieId}")
    fun getDetailMovie(@Path("movieId") movieId: String?) : Call<GetDetailMovieResponse>

    @GET("movies/name")
    fun getMovieByName(@Query("movieName") key: String): Call<GetMovieByNameResponse>

    @GET("view")
    fun getMovieHistory(@Query("movieId") movieId: String?, @Header("Authorization") token: String): Call<GetMovieHistoryResponse>

    @POST("view")
    fun view(@Body history: HistoryBody, @Header("Authorization") token: String): Call<PostMovieHistoryResponse>

    @GET("view/episode")
    fun getHistoryByEpisodeId(@Query("episodeId") episodeId: String, @Header("Authorization") token: String) : Call<PostMovieHistoryResponse>

    @GET("view/viewing")
    fun getListMovieWatching(@Header("Authorization") token: String) : Call<GetListMovieWatchingResponse>

    @GET("movies/type")
    fun getListMovieByType(@Query("type") typeStr: String) : Call<GetListMovieWatchingResponse>

    @POST("bookmarks")
    fun bookmarkMovie(@Body bookmarkBody: BookmarkBody, @Header("Authorization") token: String) : Call<BookmarkResponse>

    @DELETE("bookmarks")
    fun unbookmarkMovie(@Query("movieId") movieId: String, @Header("Authorization") token: String) : Call<BookmarkResponse>

    @GET("bookmarks/movieId")
    fun getBookmarkByMovieId(@Query("movieId") movieId: String, @Header("Authorization") token: String) : Call<GetBookmarkByMovieIdResponse>

    @GET("bookmarks")
    fun getBookmark(@Header("Authorization") token: String) : Call<GetBookmarkResponse>

}

data class GetBookmarkResponse(
    @SerializedName("error")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: List<Bookmark>
)

data class GetBookmarkByMovieIdResponse(
    @SerializedName("error")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: Boolean
)

data class BookmarkResponse(
    @SerializedName("error")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: Bookmark
)

data class Bookmark(
    @SerializedName("id")
    val id: String,
    @SerializedName("movieId")
    val movieId: String,
    @SerializedName("userId")
    val userId: String
)

data class BookmarkBody(
    @SerializedName("movieId")
    val movieId: String
)

data class GetListMovieWatchingResponse(
    @SerializedName("error")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: List<Movie>
)

data class PostMovieHistoryResponse(
    @SerializedName("errorCode")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: History
)

data class HistoryBody(
    @SerializedName("episodeId") val episodeId: String,
    @SerializedName("viewStatus") val viewStatus: Int,
    @SerializedName("currentViewing") val currentViewing: Long?
)

data class GetMovieHistoryResponse(
    @SerializedName("errorCode")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: List<History>
)

data class History(
    @SerializedName("userId")
    val userId: String,
    @SerializedName("movieId")
    val movieId: String,
    @SerializedName("episodeId")
    val episodeId: String,
    @SerializedName("viewStatus")
    val viewStatus: String?,
    @SerializedName("currentViewing")
    val currentViewing: Long?
)

data class GetMovieByNameResponse(
    val errorCode: Int?,
    val message: String?,
    val data: List<Movie>?
)

data class GetTopFilmResponse(
    val errorCode: Int?,
    val message: String?,
    val data: List<Movie>?
)

data class Movie(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String?,
    @SerializedName("subName")
    val subName: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("duration")
    val duration: String?,
    @SerializedName("director")
    val director: String?,
    @SerializedName("numEpisode")
    val numEpisode: Int?,
    @SerializedName("publishDate")
    val publishDate: String?,
    @SerializedName("updateDate")
    val updateDate: String?,
    @SerializedName("thumbnailUrl")
    val thumbnailUrl: String?,
    @SerializedName("link")
    val link: String?,
    @SerializedName("type")
    val type: List<String>?,
    @SerializedName("view")
    val view: Int?,
    @SerializedName("listEpisode")
    val listEpisode: List<Episode>?,
    @SerializedName("isBookmark")
    val isBookmark: Boolean?
)

data class Episode(
    @SerializedName("id")
    val id: String,
    @SerializedName("movieId")
    val movieId: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("link")
    val link: String?,
    @SerializedName("linkPlay")
    val linkPlay: String?
)

data class GetDetailMovieResponse(
    val errorCode: Int?,
    val message: String?,
    val data: List<Movie>?
)