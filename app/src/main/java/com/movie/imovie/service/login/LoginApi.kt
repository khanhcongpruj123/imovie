package com.movie.imovie.service

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("user/login")
    fun loginWithCredential(@Body loginBody: LoginBody): Call<LoginResponse>

    @POST("user/register")
    fun registerWithUsernameAndPassword(@Body registerBody: RegisterBody): Call<LoginResponse>
}

data class RegisterBody(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("phoneNumber")
    val phone: String
)

data class LoginBody(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String
)

data class LoginResponse(
    @SerializedName("errorCode")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("data")
    val data: DataLogin
)

data class DataLogin(
    @SerializedName("token")
    val token: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
)