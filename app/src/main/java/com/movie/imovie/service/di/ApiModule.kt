package com.movie.imovie.service.di

import retrofit2.Retrofit
import com.google.gson.Gson
import com.movie.imovie.service.LoginApi
import com.movie.imovie.service.movie.MovieApi
import retrofit2.converter.gson.GsonConverterFactory

object ApiModule {

    const val HOST = "192.168.1.9"
    const val PORT = "8090"

    private fun getRetrofit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://${HOST}:${PORT}/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
    }

    fun getLoginService(): LoginApi {
        return getRetrofit().create(LoginApi::class.java)
    }

    fun getMovieService() : MovieApi {
        return getRetrofit().create(MovieApi::class.java)
    }
}